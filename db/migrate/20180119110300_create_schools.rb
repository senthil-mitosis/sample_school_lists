class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
    	t.string :SchoolName
		t.string :GradeCtgy
		t.string :SchoolCountyName
		t.string :SchoolPhone
		t.string :SchoolLocationAddressLine1
		t.string :SchoolLocationCity
		t.string :SchoolLocationState
		t.integer :SchoolLocationZipCode
		
        t.timestamps
    end
  end
end
