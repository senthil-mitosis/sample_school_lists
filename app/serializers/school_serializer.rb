class SchoolSerializer < ActiveModel::Serializer
  attributes :id, :SchoolName, :SchoolLocationAddressLine1, :SchoolLocationCity, :SchoolLocationZipCode
end
