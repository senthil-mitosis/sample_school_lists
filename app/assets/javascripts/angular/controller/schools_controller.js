var myApp = angular.module('myapplication', ['ngRoute', 'ngResource']); 

//Factory

myApp.factory('Schools', ['$resource',function($resource){
  return $resource('/schools.json', {},{
    query: { method: 'GET', isArray: true },
    create: { method: 'POST' }
  })
}]);

myApp.factory('School', ['$resource', function($resource){
  return $resource('/schools/:id.json', {}, {
    show: { method: 'GET' },
    update: { method: 'PUT', params: {id: '@id'} },
    delete: { method: 'DELETE', params: {id: '@id'} }
  });
}]);

//Controller

myApp.controller("SchoolListCtrl", ['$scope', '$http', '$resource', 'Schools', 'School', '$location', function($scope, $http, $resource, Schools, School, $location) {

  $scope.schools = Schools.query();

  $scope.search = function(){
    if ($scope.userForm.$valid){
      Schools.query({search: $scope.search_txt},function(response){
        $scope.schools = response
      }, function(error) {
        console.log(error)
      });
    }
  };
}]);

//Routes
myApp.config([
  '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/schools',{
      templateUrl: '/templates/schools/index.html',
      controller: 'SchoolListCtrl'
    });    
    $routeProvider.otherwise({
      redirectTo: '/schools'
    });
  }
]);