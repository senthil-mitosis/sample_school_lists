class SchoolsController < ApplicationController
before_action :get_school, except: [:index, :create]
  respond_to :html, :json

  # GET /schools.json
  def index
    @school = School.all
    #@school = School.paginate(:page => params[:page], :per_page => 10)
    @school = @school.where('SchoolName like (?)', "%#{params[:search]}%") if params[:search]

    respond_with(@schools) do |format|
      format.json { render :json => @school }
      format.html
    end
  end

  def create
    @school = School.new(school_params)
    if @school.save
      render json: @school.as_json, status: :ok
    else
      render json: {school: @school.errors, status: :no_content}
    end
  end      

  def show
    respond_with(@school.as_json)
  end

  def update
    if @school.update_attributes(school_params)
      render json: @school.as_json, status: :ok 
    else
      render json: {school: @school.errors, status: :unprocessable_entity}
    end
  end

  def destroy
    @school.destroy
    render json: {status: :ok}
  end

  private

  def school_params    
    params.require(:school).permit(:SchoolName, :GradeCtgy, :SchoolCountyName, 
    	:SchoolLocationAddressLine1, :SchoolLocationCity, :SchoolLocationZipCode, :SchoolPhone)
  end

  def get_school
    @school = School.find(params[:id])
    render json: {status: :not_found} unless @school
  end
end
