require 'csv'

namespace :datas do
  desc "Import Sport Centers from CSV file"
  task school_lists: :environment do

    counter = 0
    filename = File.join Rails.root, "/lib/tasks/datas/school_lists.csv"
    
    CSV.foreach(filename, headers: true, skip_blanks: true) do |row|
      p "#{row['SchoolName']} - #{row['SchoolLocationCity']}"
      
      #p row.to_hash
      schools = School.create(row.to_hash)
      if schools.errors.any?
        except_file = File.join Rails.root, "/lib/tasks/datas/school_lists_except.csv"
        CSV.open(except_file, "a+") do |csv|
          csv << row
        end
        puts "#{row['SchoolName']} - #{schools.errors.full_messages.join(",")}"
      end
      counter += 1 if schools.persisted?      
    end

    puts "Imported #{counter} Schools"
  end
end
